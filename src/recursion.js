/* jshint esversion: 6 */

// Solve the following prompts using recursion.

// 1. Calculate the factorial of a number. The factorial of a non-negative integer n,
// denoted by n!, is the product of all positive integers less than or equal to n.
// Example: 5! = 5 x 4 x 3 x 2 x 1 = 120
// factorial(5); // 120
var factorial = function(n) {
    if (typeof(n) != 'number' || n < 0) { // error case
        return null;
    } else if (n == 0 || n == 1) { // base case
        return 1;
    } else { // recursive case
        return n * factorial(n - 1);
    }
};

// 2. Compute the sum of an array of integers.
// sum([1,2,3,4,5,6]); // 21
var sum = function(array) {
    if (! Array.isArray(array) || array.length == 0)
        return 0;

    return array[0] + sum(array.slice(1));
};

// 3. Sum all numbers in an array containing nested arrays.
// arraySum([1,[2,3],[[4]],5]); // 15
var arraySum = function(array) {
    return array.reduce((accum, current) => {
        if (Array.isArray(current)) {
            return accum + arraySum(current);
        } else {
            return Math.floor(accum) + Math.floor(current);
        }
    }, 0);
};

// 4. Check if a number is even.
var isEven = function(n) {
    if (n == 0) {
        return true;
    } else if (n == 1) {
        return false;
    } else {
        return ! isEven(n - Math.sign(n));
    }
};

// 5. Sum all integers below a given integer.
// sumBelow(10); // 45
// sumBelow(7); // 21
var sumBelow = function(n) {
    if (n == 0) {
        return 0;
    } else {
        let partial = n - Math.sign(n);
        return partial + sumBelow(partial);
    }
};

// 6. Get the integers within a range (x, y).
// range(2,9); // [3,4,5,6,7,8]
var range = function(x, y) {
    let dir = Math.sign(y - x);
    let mag = Math.abs(y - x);

    if (mag < 2) {
        return [];
    } else {
        return range(x, y - dir).concat(y - dir);
    }
};

// 7. Compute the exponent of a number.
// The exponent of a number says how many times the base number is used as a factor.
// 8^2 = 8 x 8 = 64. Here, 8 is the base and 2 is the exponent.
// exponent(4,3); // 64
// https://www.khanacademy.org/computing/computer-science/algorithms/recursive-algorithms/a/computing-powers-of-a-number
var exponent = function(base, exp) {
    if (exp == 0) {
        return 1;
    } else if (exp < 0) {
        return 1 / exponent(base, -exp);
    } else {
        if (exp % 2) {
            return base * exponent(base, exp - 1);
        } else {
            let partial = exponent(base, exp / 2);
            return partial * partial;
        }
    }
};

// 8. Determine if a number is a power of two.
// powerOfTwo(1); // true
// powerOfTwo(16); // true
// powerOfTwo(10); // false
var powerOfTwo = function(n) {
    if (n <= 1) {
        return Boolean(n);
    } else {
        let half = n / 2;
        return Number.isInteger(half) && powerOfTwo(half);
    }
};

// 9. Write a function that reverses a string.
var reverse = function(string) {
    if (string.length <= 1) {
        return string;
    } else {
        return reverse(string.slice(1)) + string.charAt();
    }
};

// 10. Write a function that determines if a string is a palindrome.
var palindrome = function(string) {
    let normal = string.trim().toLowerCase();
    if (normal.length <= 1) {
        return true;
    } else {
        return normal.slice(0, 1) == normal.slice(-1) && palindrome(normal.slice(1,-1));
    }
};

// 11. Write a function that returns the remainder of x divided by y without using the
// modulo (%) operator.
// modulo(5,2) // 1
// modulo(17,5) // 2
// modulo(22,6) // 4
var modulo = function(x, y) {
    y < 0 && (y = -y);
    let abs_x = (x < 0 ? -x : x);

    if (y == 0) {
        return NaN;
    } else if (abs_x < y) {
        return x;
    } else {
        if (x < 0) {
            return modulo(x + y, y);
        } else {
            return modulo(x - y, y);
        }
    }
};

// 12. Write a function that multiplies two numbers without using the * operator or
// Math methods.
var multiply = function(x, y) {
    if (y < 0) {
        [x, y] = [-x, -y];
    }

    if (y == 0) {
        return 0;
    } else if (y > 0) {
        return x + multiply(x, y - 1);
    }
};

// 13. Write a function that divides two numbers without using the / operator or
// Math methods to arrive at an approximate quotient (ignore decimal endings).
// *** Implementation note: I don't think the recursive case is fully general yet
var divide = function(x, y) {
    if (y == 0) return NaN;
    if (y < 0) [x, y] = [-x, -y];

    let abs_x = x < 0 ? -x : x;

    if (abs_x < y) {
        return 0;
    } else {
        return 1 + divide(abs_x - y, y);
    }
};

// 14. Find the greatest common divisor (gcd) of two positive numbers. The GCD of two
// integers is the greatest integer that divides both x and y with no remainder.
// gcd(4,36); // 4
// http://www.cse.wustl.edu/~kjg/cse131/Notes/Recursion/recursion.html
// https://www.khanacademy.org/computing/computer-science/cryptography/modarithmetic/a/the-euclidean-algorithm
var gcd = function(x, y) {
    if (x < 0 || y < 0) { // error
        return null;
    } else if (y == 0) { // base
        return x;
    } else { // recursive
        return gcd(y, x % y);
    }
};

// 15. Write a function that compares each character of two strings and returns true if
// both are identical.
// compareStr('house', 'houses') // false
// compareStr('tomato', 'tomato') // true
var compareStr = function(str1, str2) {
    if (str1.length == 0 && str2.length == 0) return true;

    return str1.charAt()==str2.charAt() && compareStr(str1.slice(1), str2.slice(1));
};

// 16. Write a function that accepts a string and creates an array where each letter
// occupies an index of the array.
var createArray = function(str) {
    if (str.length == 0) {
        return [];
    } else {
        return [str.charAt()].concat(createArray(str.slice(1)));
    }
};

// 17. Reverse the order of an array
var reverseArr = function(array) {
    if (array.length == 0) {
        return [];
    } else {
        return reverseArr(array.slice(1)).concat(array[0]);
    }
};

// 18. Create a new array with a given value and length.
// buildList(0,5) // [0,0,0,0,0]
// buildList(7,3) // [7,7,7]
var buildList = function(value, length) {
    if (length == 0) {
        return [];
    } else {
        return [value].concat(buildList(value, length - 1));
    }
};

// 19. Implement FizzBuzz. Given integer n, return an array of the string representations of 1 to n.
// For multiples of three, output 'Fizz' instead of the number.
// For multiples of five, output 'Buzz' instead of the number.
// For numbers which are multiples of both three and five, output “FizzBuzz” instead of the number.
// fizzBuzz(5) // ['1','2','Fizz','4','Buzz']
var fizzBuzz = function(n) {
    if (!Number.isInteger(n) || n <= 0)
        return [];

    let str = ((n % 3 ? '' : 'Fizz') + (n % 5 ? '' : 'Buzz')) || n.toString();

    return fizzBuzz(n - 1).concat(str);
};

// 20. Count the occurrence of a value in a list.
// countOccurrence([2,7,4,4,1,4], 4) // 3
// countOccurrence([2,'banana',4,4,1,'banana'], 'banana') // 2
var countOccurrence = function(array, value) {
    if (array.length == 0) {
        return 0;
    } else {
        return Number(array[0]===value) + countOccurrence(array.slice(1), value);
    }
};

// 21. Write a recursive version of map.
// rMap([1,2,3], timesTwo); // [2,4,6]
var rMap = function(array, callback) {
    if (array.length == 0) {
        return [];
    } else {
        return [callback(array[0])].concat(rMap(array.slice(1), callback));
    }
};

// 22. Write a function that counts the number of times a key occurs in an object.
// var obj = {'e':{'x':'y'},'t':{'r':{'e':'r'},'p':{'y':'r'}},'y':'e'};
// countKeysInObj(obj, 'r') // 1
// countKeysInObj(obj, 'e') // 2
var countKeysInObj = function(obj, key) {
    let sum = 0;
    for (let k in obj) {
        if (typeof(obj[k]) == 'object') {
            sum += countKeysInObj(obj[k], key);
        }

        if (k === key) sum++;
    }

    return sum;
};

// 23. Write a function that counts the number of times a value occurs in an object.
// var obj = {'e':{'x':'y'},'t':{'r':{'e':'r'},'p':{'y':'r'}},'y':'e'};
// countValuesInObj(obj, 'r') // 2
// countValuesInObj(obj, 'e') // 1
var countValuesInObj = function(obj, value) {
    let sum = 0;
    let key;
    if (Object.keys(obj).length > 0) {
        for (key in obj) {
            if (typeof(obj[key]) == 'object') {
                sum += countValuesInObj(obj[key], value);
            } else {
                if (obj[key] == value) sum++;
            }
        }
    }

    return sum;
};

// 24. Find all keys in an object (and nested objects) by a provided name and rename
// them to a provided new name while preserving the value stored at that key.
var replaceKeysInObj = function(obj, oldKey, newKey) {
    if (Object.keys(obj).length > 0) {
        for (let key in obj) {
            if (typeof(obj[key]) == 'object') {
                obj[key] = replaceKeysInObj(obj[key], oldKey, newKey);
            }

            if (key == oldKey) {
                obj[newKey] = obj[oldKey];
                delete obj[oldKey];
            }
        }
    }

    return obj;
};

// 25. Get the first n Fibonacci numbers. In the Fibonacci sequence, each subsequent
// number is the sum of the previous two.
// Example: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34.....
// fibonacci(5); // [0,1,1,2,3,5]
// Note: The 0 is not counted.
var fibonacci = function(n) {
    if ((typeof(n) != 'number') || (n < 1)) {
        return null;
    } else if (n == 1) {
        return [0,1];
    } else {
        let partial = fibonacci(n - 1);
        partial.push(partial[partial.length - 1] + partial[partial.length - 2]);
        return partial;
    }
};

// 26. Return the Fibonacci number located at index n of the Fibonacci sequence.
// [0,1,1,2,3,5,8,13,21]
// nthFibo(5); // 5
// nthFibo(7); // 13
// nthFibo(3); // 2
var nthFibo = function(n) {
    if ((typeof(n) != 'number') || (n < 0)) {
        return null;
    } else if (n <= 1) {
        return n;
    } else {
        return nthFibo(n - 1) + nthFibo(n - 2); // exponential time this way :-(
    }
};

// 27. Given an array of words, return a new array containing each word capitalized.
// var words = ['i', 'am', 'learning', 'recursion'];
// capitalizedWords(words); // ['I', 'AM', 'LEARNING', 'RECURSION']
var capitalizeWords = function(array) {
    if (array.length == 0) {
        return [];
    } else {
        return [array[0].toUpperCase()].concat(capitalizeWords(array.slice(1)));
    }
};

// 28. Given an array of strings, capitalize the first letter of each index.
// capitalizeFirst(['car','poop','banana']); // ['Car','Poop','Banana']
var capitalizeFirst = function(array) {
    if (array.length == 0) {
        return [];
    } else {
        return [array[0].replace(/^./, m=>m.toUpperCase())].concat(capitalizeFirst(array.slice(1)));
    }
};

// 29. Return the sum of all even numbers in an object containing nested objects.
// var obj1 = {
//   a: 2,
//   b: {b: 2, bb: {b: 3, bb: {b: 2}}},
//   c: {c: {c: 2}, cc: 'ball', ccc: 5},
//   d: 1,
//   e: {e: {e: 2}, ee: 'car'}
// };
// nestedEvenSum(obj1); // 10
var nestedEvenSum = function(obj) {
    let sum = 0;

    for (let key in obj) {
        if (typeof(obj[key]) == 'object')
            sum += nestedEvenSum(obj[key]);
        else if (Number.isInteger(obj[key]))
            (obj[key] % 2) || (sum += obj[key]);
    }

    return sum;
};

// 30. Flatten an array containing nested arrays.
// flatten([1,[2],[3,[[4]]],5]); // [1,2,3,4,5]
var flatten = function(array) {
    let result = [];
    array.forEach(e => {
        if (Array.isArray(e)) {
            result = result.concat(flatten(e));
        } else {
            result.push(e);
        }
    });

    return result;
};

// 31. Given a string, return an object containing tallies of each letter.
// letterTally('potato'); // {p:1, o:2, t:2, a:1}
var letterTally = function(str, obj = {}) {
    if (str.length == 0) {
        return obj;
    } else {
        let first = str.charAt();
        obj[first] = (obj[first] ?? 0) + 1;
        return letterTally(str.slice(1), obj);
    }
};

// 32. Eliminate consecutive duplicates in a list. If the list contains repeated
// elements they should be replaced with a single copy of the element. The order of the
// elements should not be changed.
// compress([1,2,2,3,4,4,5,5,5]) // [1,2,3,4,5]
// compress([1,2,2,3,4,4,2,5,5,5,4,4]) // [1,2,3,4,2,5,4]
var compress = function(list) {
    if (list.length == 0) {
        return [];
    } else {
        let cdr = compress(list.slice(1));
        if (list[0] != cdr[0])
            cdr.unshift(list[0]);

        return cdr;
    }
};

// 33. Augment every element in a list with a new value where each element is an array
// itself.
// augmentElements([[],[3],[7]], 5); // [[5],[3,5],[7,5]]
var augmentElements = function(array, aug) { // this is fucking stupid
    if (array.length == 0) {
        return array;
    } else if (array.length == 1) {
        array[0].push(aug);
        return array;
    } else {
        let cdr = array.splice(1);
        cdr = augmentElements(cdr, aug);
        array[0].push(aug);
        return array.concat(cdr);
    }
};

// 34. Reduce a series of zeroes to a single 0.
// minimizeZeroes([2,0,0,0,1,4]) // [2,0,1,4]
// minimizeZeroes([2,0,0,0,1,0,0,4]) // [2,0,1,0,4]
var minimizeZeroes = function(array) {
    if (array.length == 0) {
        return [];
    } else {
        let cdr = minimizeZeroes(array.slice(1));
        if (array[0] != 0 || cdr[0] != 0)
            cdr.unshift(array[0]);

        return cdr;
    }
};

// 35. Alternate the numbers in an array between positive and negative regardless of
// their original sign. The first number in the index always needs to be positive.
// alternateSign([2,7,8,3,1,4]) // [2,-7,8,-3,1,-4]
// alternateSign([-2,-7,8,3,-1,4]) // [2,-7,8,-3,1,-4]
var alternateSign = function(array) {
    if (array.length == 1) {
        return [Math.abs(array[0])];
    } else {
        let last = Math.abs(array[array.length - 1]);
        let partial = alternateSign(array.slice(0, -1));
        let sign = - Math.sign(partial[partial.length - 1]);
        return partial.concat(sign * last);
    }
};

// 36. Given a string, return a string with digits converted to their word equivalent.
// Assume all numbers are single digits (less than 10).
// numToText("I have 5 dogs and 6 ponies"); // "I have five dogs and six ponies"
var numToText = function(str) {
    let words = ['zero','one','two','three','four','five','six','seven','eight','nine'];
    let re = /(\s*)((\d)|([^\s]+))(\s+|$)/g;
    let match = re.exec(str);
    let sub = '';

    if (match == null) {
        return str;
    } else {
        if (match[3] !== undefined) {
            sub = match[1] + words[parseInt(match[2])] + match[5];
        } else {
            sub = match[0];
        }

        return sub + numToText(str.slice(re.lastIndex));
    }
};


// *** EXTRA CREDIT ***

// 37. Return the number of times a tag occurs in the DOM.
var tagCount = function(tag, node = document.body) {
    let count = 0;

    node.childNodes.forEach(e => {
        if (e.hasChildNodes())
            count += tagCount(tag, e);

        if (e.nodeName.toLowerCase() == tag.toLowerCase())
            count++;
    });

    return count;
};

// 38. Write a function for binary search.
// var array = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
// binarySearch(array, 5) // 5
// https://www.khanacademy.org/computing/computer-science/algorithms/binary-search/a/binary-search
var binarySearch = function(array, target, min, max) {
    if (arguments.length !== 4) {
        min = 0;
        max = array.length - 1;
    }

    var mid = Math.trunc(min / 2 + max / 2);
    if (array[mid] === target) {
        return mid;
    } else if (min >= max) {
        return null;
    } else if (array[mid] < target) {
        return binarySearch(array, target, mid + 1, max);
    } else {
        return binarySearch(array, target, min, mid - 1);
    }
};

// 39. Write a merge sort function.
// mergeSort([34,7,23,32,5,62]) // [5,7,23,32,34,62]
// https://www.khanacademy.org/computing/computer-science/algorithms/merge-sort/a/divide-and-conquer-algorithms
var mergeSort = function(array) {
    if (array.length <= 1) {
        return array;
    }

    let half = Math.trunc(array.length / 2);
    let left = mergeSort(array.slice(0, half));
    let right = mergeSort(array.slice(half));
    let result = [];

    while (left.length && right.length) {
        if (left[0] <= right[0]) {
            result.push(left.shift());
        } else {
            result.push(right.shift());
        }
    }

    return result.concat(left.length ? left : right);
};

// 40. Deeply clone objects and arrays.
// var obj1 = {a:1,b:{bb:{bbb:2}},c:3};
// var obj2 = clone(obj1);
// console.log(obj2); // {a:1,b:{bb:{bbb:2}},c:3}
// obj1 === obj2 // false
var clone = function(input) {
    let result;

    if (Array.isArray(input)) {
        result = [];
        for (let i = 0; i < input.length; i++) {
            result[i] = clone(input[i]);
        }
    } else if (typeof(input) == 'object') {
        result = {};
        for (let key in input) {
            result[key] = clone(input[key]);
        }
    } else {
        result = input;
    }

    return result;
};
